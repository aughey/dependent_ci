#pragma once

#include <string>

namespace local
{
    class Hello
    {
    public:
        static std::string HelloString();
        static void SayHello();
    };
}