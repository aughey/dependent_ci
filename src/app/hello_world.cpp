#include "local/Hello.h"
#if USE_LIBRARY1
#include "library1/Hello.h"
#endif
#if USE_LIBRARY2
#include "library2/Hello.h"
#endif

int main(int, char *[])
{
    local::Hello::SayHello();
#if USE_LIBRARY1
    library1::Hello::SayHello();
#endif

#if USE_LIBRARY2
    library2::Hello::SayHello();
#endif
    return 0;
}