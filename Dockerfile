# escape=`

# Use the latest Windows Server Core 2022 image.
# FROM mcr.microsoft.com/windows/servercore:ltsc2019
FROM mcr.microsoft.com/windows:20H2-amd64

# Restore the default Windows shell for correct batch processing.
SHELL ["cmd", "/S", "/C"]

COPY vs_buildtools.exe .
# Install Build Tools
RUN vs_buildtools.exe --quiet --wait --norestart --nocache `
        --installPath "%ProgramFiles(x86)%\Microsoft Visual Studio\2022\BuildTools" `
        --add Microsoft.VisualStudio.Component.Roslyn.Compiler `
        --add Microsoft.Component.MSBuild `
        --add Microsoft.VisualStudio.Component.CoreBuildTools `
        --add Microsoft.VisualStudio.Workload.MSBuildTools `
        --add Microsoft.VisualStudio.Component.Windows10SDK `
        --add Microsoft.VisualStudio.Component.VC.CoreBuildTools `
        --add Microsoft.VisualStudio.Component.VC.Tools.x86.x64 `
        --add Microsoft.VisualStudio.Component.VC.Redist.14.Latest `
        --add Microsoft.VisualStudio.Component.Windows11SDK.22621 `
        --add Microsoft.VisualStudio.Component.VC.CMake.Project `
        --add Microsoft.VisualStudio.Component.TestTools.BuildTools `
        --add Microsoft.VisualStudio.Component.VC.ATL `
        --add Microsoft.VisualStudio.Component.VC.ATLMFC `
        --add Microsoft.VisualStudio.Component.VC.14.29.16.11.MFC `
        --add Microsoft.VisualStudio.Component.VC.14.39.17.9.MFC `
        --add Microsoft.VisualStudio.Component.VC.v141.MFC `
        --add Microsoft.Net.Component.4.8.SDK `
        --add Microsoft.Net.Component.4.7.2.TargetingPack `
        --add Microsoft.VisualStudio.Component.VC.CLI.Support `
        --add Microsoft.VisualStudio.Component.VC.ASAN `
        --add Microsoft.VisualStudio.Component.TextTemplating `
        --add Microsoft.VisualStudio.Component.VC.CoreIde `
        --add Microsoft.VisualStudio.ComponentGroup.NativeDesktop.Core `
        --add Microsoft.VisualStudio.ComponentGroup.VC.Tools.142.x86.x64 `
        --add Microsoft.VisualStudio.Component.VC.v141.x86.x64 `
        --add Microsoft.Component.VC.Runtime.UCRTSDK `
        --add Microsoft.VisualStudio.Component.VC.140 `
        --add Microsoft.VisualStudio.Workload.VCTool
# RUN del /q vs_buildtools.exe

COPY cmake-3.28.1-windows-x86_64.msi .
RUN msiexec /i cmake-3.28.1-windows-x86_64.msi ALLUSERS=1 ADD_CMAKE_TO_PATH=System /qn
RUN del /q cmake-3.28.1-windows-x86_64.msi

COPY Git-2.43.0-64-bit.exe .
RUN Git-2.43.0-64-bit.exe /VERYSILENT /NORESTART /NOCANCEL /SP- /CLOSEAPPLICATIONS /RESTARTAPPLICATIONS /COMPONENTS="icons,ext\reg\shellhere,assoc,assoc_sh"
RUN del /q Git-2.43.0-64-bit.exe

COPY PowerShell-7.4.1-win-x64.msi .
RUN msiexec.exe /package PowerShell-7.4.1-win-x64.msi /quiet ENABLE_PSREMOTING=1 REGISTER_MANIFEST=1 USE_MU=1 ENABLE_MU=1 ADD_PATH=1



# Define the entry point for the docker container.
# This entry point starts the developer command prompt and launches the PowerShell shell.
#ENTRYPOINT ["C:\\Program Files (x86)\\Microsoft Visual Studio\\2022\\BuildTools\\Common7\\Tools\\VsDevCmd.bat", "&&", "powershell.exe", "-NoLogo", "-ExecutionPolicy", "Bypass"]
ENTRYPOINT ["powershell.exe"]