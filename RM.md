# Windows Build Container

## To create a container, use:

```
docker build -t buildtools:latest -m 2GB .
```

This process will build the container from the definition in Dockerfile.

## To run/build a product in the container (outside of a CI/CD system)

Assuming your source files on your local machine is in c:\sandbox

```
docker run --rm -it -v c:\sandbox:c:\sandbox buildtools
mkdir build
cd build
cmake -T v142 -A Win32 c:\sandbox\SourceDirectory
cmake --build .
```

This command will:

1. Start a docker container and map the c:\sandbox directory on the local filesystem to c:\sandbox inside the container.
    - ```--rm``` will remove the container when the shell exits.
    - ```-it``` will run an interactive version of the container.
2. Makes a (temporary build directory)
3. Changes to that directory
4. Runs ```cmake``` to create an out-of-source build of ```c:\sandbox\SourceDirectory``` building with the version 1.4.2 compiler in a Win32 platform
5. Invokes the appropriate build tool to build the source.

# Dockerfile Configuration

This specific image is build from the Microsoft provided ```mcr.microsoft.com/windows:20H2-amd64``` image.  A servercore is available at ```mcr.microsoft.com/windows/servercore:ltsc2019``` There is a [list of Windows core images](https://learn.microsoft.com/en-us/virtualization/windowscontainers/manage-containers/container-base-images) depending on your need.

```vs_buildtools.exe```, downloaded prior from Microsoft, is copied to the build container and invoked.

The options in this headless installation were pulled from a manual install of the build tools.  The GUI has the ability to export a vsconfig file that lists all of the options used.  All available compiler suites and support for MFC, ATL, CLI, and others are installed.

```cmake``` and ```git``` are installed from sres copied of the installation files.

```
PS C:\Windows\System32> Enable-WindowsOptionalFeature -Online -FeatureName containers -All

Path          :
Online        : True
RestartNeeded : False


PS C:\Windows\System32> Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Hyper-V -All

Path          :
Online        : True
RestartNeeded : False


PS C:\Windows\System32>
```

# cmake configure on aughey laptop
```
PS C:\build> Measure-Command -Expression { cmake -T v142 -A Win32 c:\source\LiveGateway |
 Out-Default }
-- Building for: Visual Studio 17 2022
-- Selecting Windows SDK version 10.0.22621.0 to target Windows 10.0.19042.
-- The C compiler identification is MSVC 19.29.30153.0
-- The CXX compiler identification is MSVC 19.29.30153.0
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Check for working C compiler: C:/Program Files (x86)/Microsoft Visual Studio/2022/Buil
dTools/VC/Tools/MSVC/14.29.30133/bin/HostX64/x86/cl.exe - skipped
-- Detecting C compile features
-- Detecting C compile features - done
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Check for working CXX compiler: C:/Program Files (x86)/Microsoft Visual Studio/2022/Bu
ildTools/VC/Tools/MSVC/14.29.30133/bin/HostX64/x86/cl.exe - skipped
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- Performing Test CMAKE_HAVE_LIBC_PTHREAD
-- Performing Test CMAKE_HAVE_LIBC_PTHREAD - Failed
-- Looking for pthread_create in pthreads
-- Looking for pthread_create in pthreads - not found
-- Looking for pthread_create in pthread
-- Looking for pthread_create in pthread - not found
-- Found Threads: TRUE
-- Found Boost: C:/source/LiveGateway/lib/boost_1_73_0 (found version "1.73.0") found com
ponents: system filesystem
-- PROGRAM_IDENT is ETLVC_GATEWAY
-- UMP_LIB is debug;C:/source/LiveGateway/lib/UMP/Win32/UMPD.lib;optimized;C:/source/Live
Gateway/lib/UMP/Win32/UMP.lib
-- DISENGINE_LIB is debug;C:/source/LiveGateway/lib/DISEngine/Win32/DISEngined.lib;optimi
zed;C:/source/LiveGateway/lib/DISEngine/Win32/DISEngine.lib
-- CCU_LIB is debug;C:/source/LiveGateway/lib/CCU/Win32/CCUD.lib;optimized;C:/source/Live
Gateway/lib/CCU/Win32/CCU.lib
-- Could NOT find Python (missing: Python_EXECUTABLE Interpreter)
-- Configuring done (28.5s)
-- Generating done (0.1s)
-- Build files have been written to: C:/build


Days              : 0
Hours             : 0
Minutes           : 0
Seconds           : 28
Milliseconds      : 723
Ticks             : 287230810
TotalDays         : 0.00033244306712963
TotalHours        : 0.00797863361111111
TotalMinutes      : 0.478718016666667
TotalSeconds      : 28.723081
TotalMilliseconds : 28723.081

```

# cmake --build .

```
Days              : 0
Hours             : 0
Minutes           : 2
Seconds           : 53
Milliseconds      : 923
Ticks             : 1739232331
TotalDays         : 0.00201300038310185
TotalHours        : 0.0483120091944444
TotalMinutes      : 2.89872055166667
TotalSeconds      : 173.9232331
TotalMilliseconds : 173923.2331
```

# cmake configure on devops build machine

```
PS C:\build> Measure-Command -Expression { cmake -T v142 -A Win32 C:\LiveGateway\LiveGateway | Out-Default }
-- Building for: Visual Studio 17 2022
-- The C compiler identification is MSVC 19.29.30154.0
-- The CXX compiler identification is MSVC 19.29.30154.0
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Check for working C compiler: C:/Program Files (x86)/Microsoft Visual Studio/2022/BuildTools/VC/Tools/MSVC/14.29.3013
3/bin/HostX64/x86/cl.exe - skipped
-- Detecting C compile features
-- Detecting C compile features - done
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Check for working CXX compiler: C:/Program Files (x86)/Microsoft Visual Studio/2022/BuildTools/VC/Tools/MSVC/14.29.30
133/bin/HostX64/x86/cl.exe - skipped
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- Performing Test CMAKE_HAVE_LIBC_PTHREAD
-- Performing Test CMAKE_HAVE_LIBC_PTHREAD - Failed
-- Looking for pthread_create in pthreads
-- Looking for pthread_create in pthreads - not found
-- Looking for pthread_create in pthread
-- Looking for pthread_create in pthread - not found
-- Found Threads: TRUE
-- Found Boost: C:/LiveGateway/LiveGateway/lib/boost_1_73_0 (found version "1.73.0") found components: system filesystem

-- PROGRAM_IDENT is ETLVC_GATEWAY
-- UMP_LIB is debug;C:/LiveGateway/LiveGateway/lib/UMP/Win32/UMPD.lib;optimized;C:/LiveGateway/LiveGateway/lib/UMP/Win32
/UMP.lib
-- DISENGINE_LIB is debug;C:/LiveGateway/LiveGateway/lib/DISEngine/Win32/DISEngined.lib;optimized;C:/LiveGateway/LiveGat
eway/lib/DISEngine/Win32/DISEngine.lib
-- CCU_LIB is debug;C:/LiveGateway/LiveGateway/lib/CCU/Win32/CCUD.lib;optimized;C:/LiveGateway/LiveGateway/lib/CCU/Win32
/CCU.lib
-- Could NOT find Python (missing: Python_EXECUTABLE Interpreter)
-- Configuring done (67.9s)
-- Generating done (0.2s)
-- Build files have been written to: C:/build


Days              : 0
Hours             : 0
Minutes           : 1
Seconds           : 8
Milliseconds      : 407
Ticks             : 684072351
TotalDays         : 0.00079175040625
TotalHours        : 0.01900200975
TotalMinutes      : 1.140120585
TotalSeconds      : 68.4072351
TotalMilliseconds : 68407.2351
```

# cmake --build on dev machine

```
Days              : 0
Hours             : 0
Minutes           : 6
Seconds           : 13
Milliseconds      : 318
Ticks             : 3733184987
TotalDays         : 0.00432081595717593
TotalHours        : 0.103699582972222
TotalMinutes      : 6.22197497833333
TotalSeconds      : 373.3184987
TotalMilliseconds : 373318.4987
```

# Distilled results

|Machine|Config Time|Build Time|Slower config|Slower build|
|-------|-----------|----------|-------------|------------|
|laptop | 28.7s     | 2.9m     | n/a         | n/a        |
|dev    | 68.4s     | 6.22m    | 2.38        | 2.14       |

> This runs all of the tests as part of the build and is not consistant with the gitlab ci.  The gitlab ci build process only creates the build artifacts and does not run the tests.

> Config and build process on bonnie for LG through CI/CD takes ~3m44s

# Build results compared to bonnie

|Machine|Total Build Time|Slower |
|-------|----------------|-------|
|laptop | 2.66m          | n/a   |
|dev    | 6.5m           | 2.5x  |
|bonnie | 3.73m          | 1.4x  |

> Note, below we run a build with all cores (4) in 3.13m.  laptop in 1.28m with 20 cores (it may be memory thrashing).

# Giving it all it can

```
C:\sandbox\wincontainer>docker run --rm -it --cpu-count=20 -v c:\sandbox:c:\sandbox -m 8GB buildtools
```

```
PS C:\> Copy-Item -Path C:\sandbox\LiveGateway\ -Destination c:\source -Recurse
```

```
PS C:\build> cmake -T v142 -A Win32 -DBUILD_GTEST_UNIT_TESTS:STRING=TRUE -DLG_ON_BUILD_SYSTEM:STRING=TRUE C:\source\LiveGateway\
```

```
Measure-Command -Expression { cmake --build . --parallel 20 | Out-Default }
```

Laptop blows through the build in 1.28 minutes pegging the CPU at 100%.  

The 4 core build dev machine runs ci build in 3.13 minutes at 100% CPU

```
git clone --depth 1 --recurse-submodules --shallow-submodules git@git-ssh.web.boeing.com:BSM/core-ios_V6_super.git
```

# Build of coreios


Days              : 0
Hours             : 0
Minutes           : 11
Seconds           : 20
Milliseconds      : 161
Ticks             : 6801612398
TotalDays         : 0.00787223657175926
TotalHours        : 0.188933677722222
TotalMinutes      : 11.3360206633333
TotalSeconds      : 680.1612398
TotalMilliseconds : 680161.2398

# Configured runner

Can you add if-not-present to the allowed_pull_policies 

configure cpus and memory in runner too

Docker needs to be started on boot, not manually on a login.

# Cloning git repository

## on BEN laptop VPN

- checkout of base core super 5.7 seconds

```
MW+M270600@A6503400 MINGW64 /c/sandbox/core-ios_V6_super (master)
$ time git submodule update --init --recursive

real    24m5.674s
user    0m0.015s
sys     0m0.031s

```

## On gitlab

Getting source stage: 47 minutes 10 seconds

With GIT_DEPTH set to 1, source time: 53:44

# /c/ProgramData/Docker/config/daemon.json

```
{
  "experimental": false,
  "hosts": [
     "npipe://","tcp://127.0.0.1:2376"
  ]
}
```

# /c/GitLab/config.toml

```
concurrent = 1
check_interval = 0
shutdown_timeout = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "BSM group runner on nosdev159vmtsgs"
  url = "https://git.web.boeing.com"
  id = 22844
  token = "glrt-LFQNN-qVH6xx1nu2ZPS_"
  token_obtained_at = 2024-03-25T13:32:21Z
  token_expires_at = 0001-01-01T00:00:00Z
  executor = "docker-windows"
  shell = "pwsh"
  SetSafeDirectoryCheckout = true
  pre_clone_script = "git config --global --add safe.directory C:/builds/BSM/core-ios_V6_super"
  [runners.cache]
    MaxUploadedArchiveSize = 0
  [runners.docker]
    tls_verify = false
        helper_image = "registry.gitlab.com/gitlab-org/gitlab-runner/gitlab-runner-helper:x86_64-bleeding-servercore1809"
        host = "tcp://127.0.0.1:2376"
    image = "a"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["c:\\gitlab-cache:c:\\gitlab-cache:rw","c:\\gitlab-work:c:\\gitlab-work:rw"]
        allowed_pull_policies = ["if-not-present","always"]
    shm_size = 0
    cpus = "4"
    memory = "8GB"
```

# To register dockerd as service

```
C:\Program Files\Docker\Docker\resources>dockerd --register-service
```