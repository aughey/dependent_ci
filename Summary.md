## GitLab CI/CD Integration for Child and Parent Projects: A Breakdown and Philosophy

This document delves into the CI/CD integration strategy between a child project (Library One) and its parent project (`dependent_ci`) using GitLab. It explores the rationale behind this approach, analyzes the functionalities within their respective CI/CD scripts (`.gitlab-ci.yml`), and explains how Library One ensures its changes don't disrupt the parent project.

### Project Structure and Context

* **Parent Project:** `dependent_ci` (assumed to use CMake for building and `make test` for running tests)
* **Child Project:** Library One (a C++ library with its own unit tests)

Our primary focus is on understanding how Library One's CI/CD script integrates with the parent project for compatibility testing.

### Philosophy of Child-Driven Integration Testing

The implemented approach leverages a child-driven philosophy for integration testing. This means:

* **Explicit Child Responsibility:** Library One's CI/CD script actively takes ownership of testing its compatibility with the parent project. This promotes a proactive approach to ensuring changes in the child project don't cause regressions in the parent.
* **Explicit Testing Stage:** The `buildparent` stage within Library One's CI/CD script is dedicated to this integration testing. This separation of concerns enhances clarity and maintainability.

### Library One's CI/CD Script Breakdown (.gitlab-ci.yml)

**Stages:**

1. **build**: Builds Library One itself.
2. **test**: Runs Library One's unit tests.
3. **buildparent** (**New Stage):** This stage focuses on integration testing with the parent project.

**General Functionality:**

* The script utilizes a matrix (`parallel`) to run builds and tests for three configurations ("one", "two", "three").
* Docker containers (specifically the `gcc` image) are employed for building and testing.

**Build Stage (Lines 1-42):**

* Updates package lists and installs `cmake`.
* Builds the library using `cmake` and `make`.
* Creates artifacts: The built test executables located in `build/tests/tests`.

**Test Stage (Lines 44-54):**

* Runs the built test executables with `--output-on-failure`.
* Depends on the `build` stage to ensure tests run on a successful build.

**Buildparent Stage (Lines 44-64):**

This stage is crucial for integration testing with the `dependent_ci` project. Here's a breakdown:

* Sets `GIT_STRATEGY: none` to avoid checking out Library One itself in this stage (focusing on the parent project).
* Clones the parent project (`dependent_ci`) repository into a `parent-repo` directory. (The URL could be a variable for better maintainability)
* Runs the `mergetest.sh` script, which likely performs these actions:
    * Takes arguments:
        * `$CI_PROJECT_NAME`: Name of the current project (Library One)
        * `$CI_COMMIT_SHA`: Commit hash of the current merge request
        * `$CONFIG`: Configuration value ("one", "two", or "three") for the parent project's build
    * Builds the parent project with the newly built Library One at the specified configuration.
    * Checks for any build failures or compatibility issues in the parent project.
* Runs conditionally:
    * When the pipeline is triggered by a merge request (`$CI_PIPELINE_SOURCE == "merge_request_event"`).
    * OR when manually triggered.
* Allows failures (`allow_failure: true`) during this integration testing stage, as unexpected breakages might occur due to changes in Library One.

### `mergetest.sh` Script Breakdown (Located in `dependent_ci` Project)

**Arguments:**

* `$1`: Name of the child submodule (Library One in this case)
* `$2`: Commit hash of the merge request in the child submodule
* `$3`: Configuration value ("one", "two", or "three") for the parent project's build

**Process:**

1. **Echo Information:** Prints messages about the submodule, commit hash, and build configuration.
2. **Submodule Update (Line 10):** Performs a more comprehensive submodule update using `git submodule update --depth 1 --init` to ensure all submodules within Library One are updated.
3. **Navigate and Update Child Submodule (Lines 11-12):**
    * Goes into the child submodule directory (`$PROJ`) within the parent project's workspace.
    
## Additional Dependencies for Windows Builds

Following the installation of Visual Studio Build Tools, the Dockerfile incorporates several additional tools crucial for development workflows within the container:

* **CMake (Lines 40-42):**
    * CMake, a popular cross-platform build system generator, is installed using the `cmake-3.28.1-windows-x86_64.msi` installer.
    * The installation process is silent (`/qn`) and configures CMake for all users (`ALLUSERS=1`), adding it to the system path (`ADD_CMAKE_TO_PATH=System`) for convenient access.

* **Git (Lines 43-45):**
    * Git, a widely used version control system, is installed using the `Git-2.43.0-64-bit.exe` installer.
    * The installation is silent (`/VERYSILENT`) and includes specific components:
        * Icons for visual representation.
        * Shell extensions for Git integration within the file explorer.
        * File associations for seamless Git interaction.
    * System restart is avoided (`/NORESTART`) to streamline the process.

* **PowerShell (Lines 46-51):**
    * PowerShell, a powerful scripting language and command-line shell, is installed using the `PowerShell-7.4.1-win-x64.msi` installer.
    * Similar to other tools, the installation is silent (`/quiet`).
    * Additional configuration enables features like remote access (PSRemoting) and multi-user functionality.
    * Importantly, PowerShell is added to the system path (`ADD_PATH=1`) within the container for easy execution.

**Key Points:**

* These installers are likely obtained from a private repository for consistency and potentially to manage specific versions.
* The chosen arguments during installation prioritize silent operation and configuration tailored to the container's environment.

By incorporating these additional tools alongside Visual Studio Build Tools, the Dockerfile creates a versatile Windows container image suitable for various build workflows. This image provides a pre-configured development environment, improving the efficiency and consistency of CI builds on Windows machines.



## Building and Testing Docker Containers for CI/CD

This document outlines the process of building and testing Docker containers for a CI/CD pipeline. It covers key concepts, configurations, and considerations for efficient containerized builds.

### Building the Container

1. **Dockerfile:** The provided Dockerfile defines the base image (`mcr.microsoft.com/windows:20H2-amd64`) and installs essential tools:
    * Visual Studio Build Tools
    * CMake
    * Git
    * PowerShell

2. **Building the Image:** Use the following command to build the container image:

   ```bash
   docker build -t <name> -m <memory> .
   ```

   * Replace `<name>` with a desired name or tag for your image (e.g., `buildtools:latest`).
   * Replace `<memory>` with the amount of memory you want to allocate to the container (e.g., `2GB`).
   * The `.` specifies the current directory where the Dockerfile resides.

**Prerequisites:** Ensure Docker is installed and configured for Windows containers. Refer to Docker's documentation for setup instructions if needed.

### Testing the Build Manually

1. **Running the Container:** The following commands demonstrate how to run the container and execute a build process:

   ```bash
   docker run --rm -it -v <local-dir>:<container-dir> <container-name>
   ```

   * `--rm`: This option tells Docker to remove the container after it exits.
   * `-it`: This option provides an interactive terminal within the container.
   * `-v <local-dir>:<container-dir>`: This option maps a directory from your local filesystem (`<local-dir>`) to a directory inside the container (`<container-dir>`).
   * `<container-name>`: This specifies the name of the container image to run (e.g., `buildtools`).

2. **Build Simulation:** In this scenario, you're manually simulating a CI/CD build process. You have the necessary source code on your local machine and want to test the build functionality within the container. This helps you identify and refine the build commands before integrating them into an automated pipeline.

**Note:** The `-v` option maps the specified directory from your local machine to the container's filesystem. This creates a link, but the actual files reside on your local storage. The build process likely creates and stores temporary or output files within the mapped local directory.

### Dockerfile Configuration and Host System Considerations

The Dockerfile specifies a base image tailored to a specific Windows Server version. Ideally, containers should be portable across different host systems. However, in this case, the chosen tools and their dependencies might require a specific Windows version for compatibility.

The documentation references internet-available resources for Windows container base images. Your private network might offer its own container image registry with these resources available internally. Consider using these private resources if applicable.

While this specific setup uses a version-specific base image, explore these approaches for improved portability in future projects:

* **Multi-stage builds:** Use a base image with minimal dependencies in the first stage to build the application. Then, copy the application binary into a smaller, more portable image in a subsequent stage.
* **Newer base images:** Newer Windows base images may offer broader compatibility with various tools and libraries.
* **Alternative tools:** Research tools with wider compatibility across Windows versions.

### Enabling Windows Features for Docker

The `Enable-WindowsOptionalFeature` PowerShell commands ensure essential features are enabled on the host system before running Docker containers:

* **Containers:** This feature is required for running Docker containers.
* **Hyper-V:** This feature provides virtualization capabilities used by Docker.

Ideally, you would have a pre-configured host system image that already has these features enabled. This eliminates the need to run these commands manually.

Consider using container orchestration platforms like Kubernetes that can manage underlying infrastructure, potentially mitigating the need for manual feature activation on individual host machines.

### Benchmarking Build Performance

The `measure-command` PowerShell cmdlet is used to benchmark build times for a local project. This helps assess build performance under different configurations and identify potential bottlenecks.

The commands typically involve running `cmake configure` and `cmake --build` with specific options to configure and execute the build process. The focus is on comparing build times between your local workstation and a DevOps build machine to evaluate the efficiency of both environments.

Several factors can affect build time:

* **Hardware Differences:** The build machine might have more powerful CPUs, memory, or storage compared to your workstation.
* **Test Execution:** Some build configurations might include test execution during the build process, which can significantly impact total time.
* **Network Overhead:** If the DevOps build machine resides on a different network segment, there might be additional network latency affecting build