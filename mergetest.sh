PROJ=$1
SHA=$2
CONFIG=$3

echo "Merge test build for submodule $PROJ"
echo "Updating submodule to sha has $SHA"
echo "Building configuration $CONFIG"

# Do our top level submodule update (git checkout didn't)
git submodule update --depth 1 --init
# Go into the project directory and update to the given sha
(cd $PROJ && git fetch origin $SHA && git checkout -q $SHA)

# Run our normal build/test
mkdir -p build
cd build
cmake .. -DCONFIG=$CONFIG && make && make test
